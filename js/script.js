var app = angular.module("GloablGTMApp", []);
app.controller("GloablGTMController", function ($scope, $sce) {
  $scope.latestNews = [
    {
      URL: "./img/latestNews1.jpg",
      Description:
        "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>",
    },
    {
      URL: "./img/latestNews2.jpg",
      Description:
        "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>",
    },
  ];

  $scope.latestAnnouncements = [
    {
      Date: "06/11/2020",
      Description:
        "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>",
    },
    {
      Date: "06/18/2020",
      Description:
        "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>",
    },
    {
      Date: "06/21/2020",
      Description:
        "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>",
    },
    {
      Date: "06/28/2020",
      Description:
        "<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>",
    }
  ];

  $scope.latestDocument = [
    {
        imgURL: "./img/powerpoint.jpg",
        Date: "06/11/2020",
        Title: "Lorem Ipsum",
        Owner: "Name",
        DateModified: "07/02/2020",
        ModifiedBy: "Name"
    },
    {
        imgURL: "./img/excel.jpg",
        Date: "06/11/2020",
        Title: "Lorem Ipsum",
        Owner: "Name",
        DateModified: "07/03/2020",
        ModifiedBy: "Name"
    },
    {
        imgURL: "./img/word.jpg",
        Date: "06/11/2020",
        Title: "Lorem Ipsum",
        Owner: "Name",
        DateModified: "07/04/2020",
        ModifiedBy: "Name"
    }
  ];

    $scope.rightLinks = [
        {
            ID: "QuickLinks",
            href: "rightQuickLinks",
            MethodId: "QuLi",
            imgURL: "./img/quickLinks.jpg",
            Name: "Quick Links"
        },    
        {
            ID: "GTMEnablementContent",
            href: "rightGTMEnablementContent",
            MethodId: "GTME",
            imgURL: "./img/GTMEnablementContent.jpg",
            Name: "GTM Enablement Content"
        },
        {
            ID: "Solutions",
            href: "rightSolutions",
            MethodId: "SOLU",
            imgURL: "./img/Solutions.jpg",
            Name: "Solutions"
        },
        {
            ID: "OADocument",
            href: "rightOADocument",
            MethodId: "OADO",
            imgURL: "./img/OADocument.jpg",
            Name: "Opportunity Acceleration Documents"
        }
    ];

    $scope.toggleClass = function(id) {
        var anchorID = "#" + id;
        var plus = anchorID + ".plus";
        var minus = anchorID + ".minus";
        $(plus).toggleClass("d-none");
        $(minus).toggleClass("d-none");
    }

  $scope.ashtml = function (html) {
    return $sce.trustAsHtml(html);
  };
});
app.directive('header', ['$window', function ($window) {
    return {
       restrict: 'E',
       template: `
       <div class="row">
        <div class="col-xs-1 col-md-2 col-lg-1 pz-3 py-4">
           <a class="w-25" href="#"><img src="./img/ey.svg" alt="Home"></a>
        </div>
        <div class="col-xs-11 col-md-10 col-lg-11 pt-4 pb-1">
           <div class="row">
               <div class="col-xs-12 col-md-12 col-lg-12 pl-1">
                   <h2 class="text-light">MS Alliance Internal Global Site</h2>
               </div>
           </div>
           <div class="row">
               <div class="col-xs-12 col-md-12 col-lg-12 p-0">
                   <nav class="navbar navbar-expand-sm navbar-dark text-light p-0">   
                       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                           <span class="navbar-toggler-icon"></span>
                       </button>
                       <div class="collapse navbar-collapse text-light" id="collapsibleNavbar">
                           <ul class="navbar-nav">
                               <li class="nav-item">
                                   <a class="nav-link" href="#">Home</a>
                               </li>
                               <li class="nav-item">
                                   <a class="nav-link" href="#">Acquistions</a>
                               </li>
                               <li class="nav-item">
                                   <a class="nav-link text-warning" href="#">Alliances</a>
                               </li>
                               <li class="nav-item">
                                   <a class="nav-link" href="#">Contacts</a>
                               </li>
                           </ul>
                       </div>
                   </nav>
               </div>
           </div>
       </div>
   </div>`
    };
}]);

app.directive('mobileHeader', ['$window', function ($window) {
    return {
       restrict: 'E',
       template: `
       <div class="row">
    <div class="w-100 px-2">
        <a href="#"><img src="./img/m-ey.svg" width="95%" alt="Home"></a>
    </div>
</div>
<div class="row">
    <div class="w-75">
        <h2 id="MainTitle" class="pl-2 text-light">MS Alliance Internal Global Site</h2>
    </div>
    <div class="w-25">
        <nav class="navbar navbar-expand-sm navbar-dark">   
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav text-light">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Acquistions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-warning" href="#">Alliances</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Contacts</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div> `
    };
}]);
var vid = document.getElementById("samplevideo"); 

function playVid() { 
    vid.play(); 
} 

function pauseVid() { 
    vid.pause(); 
}